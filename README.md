# Backpack Automatic Refresher Redux

### Before using this script, you must be familiar with basic commands and follow these instructions.
By basic commands, I mean knowing what `cd` does. https://www.geeksforgeeks.org/cd-cmd-command/

---

1. Make sure you have Python **3** installed. Get it here: https://www.python.org/downloads/
    - If you're on Windows, make sure the **"Add to PATH"** option is checked when installing Python **3**.

2. Install the requirements (colorama, requests)
    - Using the command line: `pip install colorama requests`

3. Put your **Backpack.tf** token inside the script. I've put a comment that tells you where you need to add it and where to get it.
    - NOTE: The token is **NOT** your API key.
---
### Running the script
- Using cmd, Powershell or Terminal: `python3 refreshbp.py`
- If this doesn't work:
    - On Windows, make sure **"Add to PATH"** was checked when installing Python **3**.
    - Make sure you're in the folder the script is in when running the command.
    - Your version of Python **3** must be up to date, uninstall any old version.

